<?php
require dirname(__DIR__) . '/vendor/autoload.php';
session_start();

error_reporting(E_ALL);
set_error_handler('Mvc\Error::errorHandler');
set_exception_handler('Mvc\Error::exceptionHandler');

$env = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$env->load();

$router = new Mvc\Router();

$router->add('/', ['controller' => 'Tasks', 'action' => 'index']);
$router->add('/create', ['controller' => 'Tasks', 'action' => 'create']);
$router->add('/admin', ['controller' => 'Admin', 'action' => 'form']);
$router->add('/admin/login', ['controller' => 'Admin', 'action' => 'login']);
$router->add('/admin/cabinet', ['controller' => 'Admin', 'action' => 'cabinet']);
$router->add('/admin/logout', ['controller' => 'Admin', 'action' => 'logout']);
$router->add('/admin/edit', ['controller' => 'Admin', 'action' => 'edit']);
$router->add('/admin/update', ['controller' => 'Admin', 'action' => 'update']);

$router->dispatch();
