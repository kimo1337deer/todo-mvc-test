<?php

namespace Mvc;

class Router
{
    protected $routes = [];
    protected $params = [];

    public function add($route, $params = [])
    {
        $route = '/^' . preg_replace('/\//', '\\/', $route) . '$/i';
        $this->routes[$route] = $params;
    }

    public function match($url)
    {
        foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $matches)) {
                foreach ($matches as $key => $match) {
                    if (is_string($key)) {
                        $params[$key] = $match;
                    }
                    $this->params = $params;
                    return true;
                }
            }
        }

        return false;
    }

    protected function removeQueryStringVariables($url)
    {
        if ($url != '') {
            $parts = explode('&', $url, 2);

            if (strpos($parts[0], '=') === false) {
                $url = $parts[0];
            } else {
                $url = '';
            }
        }

        return $url;
    }

    public function dispatch()
    {
        $url = $this->removeQueryStringVariables(strtok($_SERVER["REQUEST_URI"], '?'));

        if ($this->match($url)) {
            $controller = $this->params['controller'];
            $controller = 'App\Controllers\\' . $controller;

            if (class_exists($controller)) {
                $controller_object = new $controller(array_merge($_SERVER, $_REQUEST));
                $action = $this->params['action'];

                if (method_exists($controller_object, $action)) {
                    $response = $controller_object->$action();

                    if ($response) {
                        echo $response;
                    }
                } else {
                    throw new \Exception("Method $action in controller $controller is not found");
                }
            } else {
                throw new \Exception("Controller class $controller not found");
            }
        } else {
            throw new \Exception('No route matched.', 404);
        }
    }
}
