<?php

namespace Mvc;

abstract class Controller
{

    protected $request = [];

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function __call($method, $args)
    {
        if (method_exists($this, $method)) {
            call_user_func_array([$this, $method], $args);
        } else {
            throw new \Exception("Method $method not found in controller " . get_class($this));
        }
    }

    protected function json($data = [], $status = 200, $success = true)
    {
        header('Content-Type: application/json');
        http_response_code($status);
        return json_encode(['success' => $success, 'data' => $data], TRUE);
    }

}
