<?php

namespace Mvc;


class Error
{
    public static function errorHandler($level, $message, $file, $line)
    {
        if (error_reporting() !== 0) {
            throw new \ErrorException($message, 0, $level, $file, $line);
        }
    }

    public static function exceptionHandler($exception)
    {
        http_response_code($exception->getCode());
        $class = get_class($exception);

        echo <<< EOT
            <h1>Fatal error</h1>
            <p>Uncaught exception: ' {$class} '</p>
            <p>Message: ' {$exception->getMessage()} '</p>
            <p>Stack trace:<pre> {$exception->getTraceAsString()}</pre></p>
            <p>Thrown in '{$exception->getFile()}' on line {$exception->getLine()}</p>
        EOT;
    }
}
