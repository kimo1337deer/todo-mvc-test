<?php

namespace Mvc;

use PDO;

abstract class Model
{
    protected static function getDB()
    {
        static $db = null;

        if ($db === null) {
            $dsn = 'mysql:host=' . getenv('DB_HOST') . ';dbname=' . getenv('DB_NAME') . ';charset=utf8';
            $db = new PDO($dsn, getenv('DB_USER'), getenv('DB_PASSWORD'));
        }

        return $db;
    }

    private static function validateText($value)
    {

        if (filter_var($value, FILTER_VALIDATE_INT)) {
            return "Input must be text";
        }

        return false;
    }

    private static function validateEmail($value)
    {

        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return "Input must be email";
        }

        return false;
    }

    private static function validateLength($value, $max)
    {
        if (strlen($value) > $max) {
            return "Length cannot be more than $max";
        }

        return false;
    }

    public static function validate($required, $request)
    {
        $invalid = [];

        foreach ($required as $field => $rules) {
            if (!array_key_exists($field, $request) || empty($request[$field])) {
                $invalid[$field] = ['Field is missing'];
            } else {
                $messages = [];
                $rules = explode("|", $rules);

                foreach ($rules as $rule) {
                    $nameAndParam = explode(":", $rule);
                    $name = array_shift($nameAndParam);
                    $param = array_pop($nameAndParam);
                    $method = 'validate' . ucfirst($name);

                    if ($param) {
                        $validated = self::$method($request[$field], $param);
                    } else {
                        $validated = self::$method($request[$field]);
                    }

                    if ($validated) {
                        array_push($messages, $validated);
                    }
                }

                if (count($messages) > 0) $invalid[$field] = $messages;
            }
        }

        return $invalid;
    }
}
