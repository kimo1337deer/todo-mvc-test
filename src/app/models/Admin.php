<?php


namespace App\Models;

use PDO;
use \Mvc\Model as Model;

class Admin extends Model
{
    public static $required = [
        'login' => 'length:45',
        'password' => 'length:45'
    ];

    public static function authenticated()
    {
        return array_key_exists('auth', $_SESSION);
    }

    public static function authenticate($request)
    {
        if ($request['login'] !== getenv('ADMIN_LOGIN')) {
            return ['login' => ['Wrong credentials']];
        }
        if ($request['password'] != getenv('ADMIN_PASSWORD')) {
            return ['password' => ['Wrong password']];
        }

        $_SESSION['auth'] = true;

        return false;
    }

    public static function logout()
    {
        unset($_SESSION['auth']);
    }
}
