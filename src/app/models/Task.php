<?php

namespace App\Models;

use PDO;
use \Mvc\Model as Model;

class Task extends Model
{
    public static $required = [
        'name' => 'text|length:30',
        'email' => 'email|length:45',
        'text' => 'length:200'
    ];

    public static function sortAndPaginate($request)
    {
        $db = static::getDB();

        $perPage = 3;
        $page = isset($request['page']) ? $request['page'] : 1;

        $stmt = $db->query('SELECT COUNT(*) as total FROM tasks');
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $total = array_pop($result);
        $total = $total['total'];

        $totalPages = ceil($total / $perPage);

        $statement = "SELECT * FROM tasks ";

        $sortParams = [];

        if (isset($request['sort']) && !empty($request['sort'])) {
            $field = $request['sort'];
            $dir = isset($request['dir']) ? $request['dir'] : 'desc';
            $formatted = strtoupper($dir);
            $statement = "$statement ORDER BY $field $formatted";
            $sortParams = [
                'dir' => $dir,
                'sort' => $field
            ];
        }

        if ($page > $totalPages) {
            $page = 1;
        }

        $paginatorParams = [
            'total' => $total,
            'totalPages' => $totalPages,
            'page' => $page
        ];

        $startAt = $perPage * ($page - 1);
        $stmt = $db->query("$statement LIMIT $startAt, $perPage");

        return array_merge($paginatorParams, $sortParams, ['tasks' => $stmt->fetchAll(PDO::FETCH_ASSOC)]);
    }

    public static function getById($id)
    {
        $db = static::getDB();
        $stmt = $db->query("SELECT * FROM tasks Where id=$id");
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return array_pop($result);
    }

    public static function create($email, $name, $text)
    {
        $db = static::getDB();
        $sql = "INSERT INTO tasks (email, name, text) VALUES (:email, :name, :text)";
        $stmt = $db->prepare($sql);
        return $stmt->execute([
            'email' => $email,
            'name' => $name,
            'text' => $text,
        ]);
    }

    public static function update($request, $edited = false)
    {
        $db = static::getDB();
        $sql = "UPDATE tasks SET email=:email, name=:name, text=:text, edited=:edited, status=:status WHERE id=:id";
        $stmt = $db->prepare($sql);
        return $stmt->execute([
            'email' => $request['email'],
            'name' => $request['name'],
            'text' => $request['text'],
            'status' => $request['status'],
            'edited' => $edited ? 1 : 0,
            'id' => $request['id']
        ]);
    }

    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM tasks');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
