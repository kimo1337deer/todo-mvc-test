<?php


namespace App\Controllers;

use App\Models\Task;
use App\Models\Admin as User;
use Mvc\Model;
use \Mvc\View as View;

class Admin extends \Mvc\Controller
{
    public function form()
    {
        if (User::authenticated()) {
            header('Location: /admin/cabinet');
        } else {
            View::render('Admin/login.html');
        }
    }

    public function cabinet()
    {
        if (!User::authenticated()) {
            header('Location: /admin');
        } else {
            View::render('Admin/index.html', array_merge(Task::sortAndPaginate($this->request), [
                'admin' => true
            ]));
        }
    }

    public function login()
    {
        $errors = Model::validate(User::$required, $this->request);

        if (count($errors) > 0) {
            View::render('Admin/login.html', [
                'errors' => $errors,
                'values' => $this->request
            ]);
        } else {
            $authErrors = User::authenticate($this->request);

            if ($authErrors) {
                View::render('Admin/login.html', [
                    'errors' => $authErrors,
                    'values' => $this->request
                ]);
            } else {
                header('Location: /admin/cabinet');
            }
        }
    }

    public function update()
    {
        if (!User::authenticated()) {
            header('Location: /admin');
            return;
        }

       if( Task::update($this->request, Task::getById($this->request['id'])['text'] !== $this->request['text'])) {
           header('Location: /admin/cabinet');
       }
    }

    public function logout()
    {
        if (User::authenticated()) {
            User::logout();
        }

        header('Location: /admin');
    }

    public function edit()
    {
        if (!User::authenticated()) {
            header('Location: /admin');
            return;
        }

        if (!array_key_exists('id', $this->request)) {
            header('Location: /admin/cabinet');
        }

        $task = Task::getById($this->request['id']);

        if (!$task) {
            header('Location: /admin/cabinet');
        }

        View::render('Admin/edit.html', [
            'task' => $task,
            'statuses' => ['new', 'suspended', 'finished']
        ]);
    }


}