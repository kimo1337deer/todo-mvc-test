<?php

namespace App\Controllers;

use App\Models\Task;
use Mvc\Model;
use \Mvc\View as View;

class Tasks extends \Mvc\Controller
{
    public function index()
    {
        View::render('Task/index.html',
            array_merge(
                Task::sortAndPaginate($this->request),
                ['new' => array_key_exists('new', $this->request)]
            ));
    }

    public function form()
    {
        View::render('Task/create.html');
    }

    public function create()
    {
        if ($this->request['REQUEST_METHOD'] === 'GET') return View::render('Task/create.html');

        $errors = Model::validate(Task::$required, $this->request);

        if (count($errors) > 0) {
            View::render('Task/create.html', [
                'errors' => $errors,
                'values' => $this->request
            ]);
        } else {
            $task = Task::create(
                $this->request['email'],
                $this->request['name'],
                $this->request['text']
            );

            header('Location: /?new');
        }
    }
}